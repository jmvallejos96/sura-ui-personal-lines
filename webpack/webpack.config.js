const webpack = require('webpack');
const path = require('path');
const config = require('../config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Stylish = require('webpack-stylish');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: '../src/index.html',
  filename: 'index.html',
  inject: 'body',
});
const ExtractTextPluginConfig = new ExtractTextPlugin({
  filename: 'index.css',
});

module.exports = {
  devServer: {
    hot: true,
    port: 8081,
    historyApiFallback: true,
  },
  context: __dirname,
  entry: ['react-hot-loader/patch', '../src/index.js'],
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loaders: ['react-hot-loader/webpack', 'babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      {
        test: /\.less|.css$/,
        use: [
          { loader: 'style-loader' }, // creates style nodes from JS strings
          { loader: 'css-loader' }, // translates CSS into CommonJS
          { loader: 'less-loader' }, // compiles Less to CSS
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    HtmlWebpackPluginConfig,
    ExtractTextPluginConfig,
    new Stylish(),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, '../src/assets/images'),
        to: path.join(__dirname, '../build/images'),
        force: true,
      },
    ]),
    new webpack.EnvironmentPlugin({
      STAGE: undefined,
      ENV: undefined,
      URL_API: config.URL_API,
      INSTANCE: 1,
    }),
  ],
  devtool: 'cheap-module-eval-source-map',
};
