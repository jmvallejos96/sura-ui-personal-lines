#### Start dev
``` STAGE=local ENVIRONMENT=development yarn start ```

### Deploy DEV
``` STAGE=DEV ENVIRONMENT=development yarn build && aws s3 sync --delete build/ s3://dev-sura-ui-personal_lines --acl public-read ```

### Deploy PROD
