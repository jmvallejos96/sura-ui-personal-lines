import defaultConfig from '../config';

var config = {
  URL_API: process.env.URL_API ? process.env.URL_API : defaultConfig.URL_API,
  API_KEY: process.env.API_KEY ? process.env.API_KEY : defaultConfig.API_KEY,
};

export default config;
