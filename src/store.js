import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const initialState = {
  issue: null,
};

const reducer = (state = initialState, action) => {
  if (action.type === 'RESET') {
    return action.initialState;
  }

  if (action.type === 'GET_ISSUE') {
    return { ...state, issue: action.issue };
  }

  return state;
};

export default createStore(reducer, window.REDUX_DATA || initialState, applyMiddleware(thunk));
